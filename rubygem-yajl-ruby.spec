%global _empty_manifest_terminate_build 0
%global gem_name yajl-ruby
Name:		rubygem-yajl-ruby
Version:	1.4.1
Release:	2
Summary:	Ruby C bindings to the excellent Yajl JSON stream-based parser library.
License:	MIT
URL:		http://github.com/brianmario/yajl-ruby
Source0:	https://rubygems.org/gems/yajl-ruby-1.4.1.gem

BuildRequires:	ruby rsync
BuildRequires:	ruby-devel
BuildRequires:	rubygems
BuildRequires:	rubygems-devel
BuildRequires:	gcc
BuildRequires:	gdb
Provides:	rubygem-yajl-ruby

%description
Ruby C bindings to the excellent Yajl JSON stream-based parser library.

%package help
Summary:	Development documents and examples for yajl-ruby
Provides:	rubygem-yajl-ruby-doc
BuildArch: noarch

%description help
Ruby C bindings to the excellent Yajl JSON stream-based parser library.

%prep
%autosetup -n yajl-ruby-1.4.1
gem spec %{SOURCE0} -l --ruby > yajl-ruby.gemspec

%build
gem build yajl-ruby.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* %{buildroot}%{gem_dir}/
rsync -a --exclude=".*" .%{gem_dir}/* %{buildroot}%{gem_dir}/
if [ -d .%{_bindir} ]; then
	mkdir -p %{buildroot}%{_bindir}
	cp -a .%{_bindir}/* %{buildroot}%{_bindir}/
fi
if [ -d ext ]; then
	mkdir -p %{buildroot}%{gem_extdir_mri}/yajl
	if [ -d .%{gem_extdir_mri}/%{gem_name} ]; then
		cp -a .%{gem_extdir_mri}/%{gem_name}/*.so %{buildroot}%{gem_extdir_mri}/yajl
	else
		cp -a .%{gem_extdir_mri}/yajl/*.so %{buildroot}%{gem_extdir_mri}/yajl
fi
	cp -a .%{gem_extdir_mri}/gem.build_complete %{buildroot}%{gem_extdir_mri}/
	rm -rf %{buildroot}%{gem_instdir}/ext/
fi
rm -rf  %{buildroot}%{gem_instdir}/{.codeclimate.yml,.gitignore,.rspec,.travis.yml}
pushd %{buildroot}
touch filelist.lst
if [ -d %{buildroot}%{_bindir} ]; then
	find .%{_bindir} -type f -printf "/%h/%f\n" >> filelist.lst
fi
popd
mv %{buildroot}/filelist.lst .

%files -n rubygem-yajl-ruby -f filelist.lst
%dir %{gem_instdir}
%{gem_instdir}/*
%{gem_extdir_mri}
%exclude %{gem_cache}
%{gem_spec}

%files help
%{gem_docdir}/*

%changelog
* Wed Aug 25 2021 wutao <wutao61@huawei.com> - 1.4.1-2
- Move .so file from /usr/lib64/gems/ruby/yajl-ruby-1.4.1/yajl-ruby/
  to /usr/lib64/gems/ruby/yajl-ruby-1.4.1/yajl/ to solve fluentd loaderror problem

* Mon Aug 02 2021 Ruby_Bot <Ruby_Bot@openeuler.org>
- Package Spec generated
